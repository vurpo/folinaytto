// include the library code:
#include <LiquidCrystal.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti WiFiMulti;

WiFiClient client;
char buffer_[80];

uint8_t aao[6][8] = {
    {
      0b00100,
      0b01010,
      0b00100,
      0b01110,
      0b10001,
      0b11111,
      0b10001,
      0b10001
    },
    {
      0b00100,
      0b01010,
      0b00100,
      0b01110,
      0b00001,
      0b01111,
      0b10001,
      0b01111
    },
    {
      0b01010,
      0b00000,
      0b00100,
      0b01010,
      0b10001,
      0b11111,
      0b10001,
      0b10001
    },
    {
      0b00000,
      0b01010,
      0b00000,
      0b01110,
      0b00001,
      0b01111,
      0b10001,
      0b01111
    },
    {
      0b01010,
      0b00000,
      0b01110,
      0b10001,
      0b10001,
      0b10001,
      0b10001,
      0b01110
    },
    {
      0b00000,
      0b01010,
      0b00000,
      0b01110,
      0b10001,
      0b10001,
      0b10001,
      0b01110
    }
};

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 15, en = 2, d4 = 16, d5 = 12, d6 = 14, d7 = 13;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);

  lcd.createChar(1, aao[0]);
  lcd.createChar(2, aao[1]);
  lcd.createChar(3, aao[2]);
  lcd.createChar(4, aao[3]);
  lcd.createChar(5, aao[4]);
  lcd.createChar(6, aao[5]);
  
  // Print a message to the LCD.
  lcd.print("Not connected");
  
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("Turku Hacklab", "hacklab666");
}

void loop() {
    // wait for WiFi connection
    if((WiFiMulti.run() == WL_CONNECTED)) {

        if (client.connect("192.168.1.15", 1337)) {
          char buffer1[21];
          char buffer2[21];
          char buffer3[21];
          char buffer4[21];
          client.readBytesUntil(0, buffer1, 21);
          buffer1[20] = 0;
          client.readBytesUntil(0, buffer2, 21);
          buffer2[20] = 0;
          client.readBytesUntil(0, buffer3, 21);
          buffer3[20] = 0;
          client.readBytesUntil(0, buffer4, 21);
          buffer4[20] = 0;
          lcd.setCursor(0,0);
          lcd.print(buffer1);
          lcd.setCursor(0,1);
          lcd.print(buffer2);
          lcd.setCursor(0,2);
          lcd.print(buffer3);
          lcd.setCursor(0,3);
          lcd.print(buffer4);
        }
        
    } else {
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Not connected");
    }

    delay(1000);
}

