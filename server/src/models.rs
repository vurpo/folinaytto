use serde_derive::Deserialize;

#[derive(Deserialize)]
pub struct SMResponse {
    pub result: Vec<Stop>
}

#[derive(Deserialize)]
pub struct Stop {
    pub lineref: String,
    pub destinationdisplay: String,
    pub expectedarrivaltime: i64,
}