use std::io::Write;
use std::net::TcpListener;
use std::net::TcpStream;
use std::thread;
use std::sync::{Arc,Mutex};
use std::time;

use chrono::{DateTime, TimeZone, Utc, Local};
use pad::PadStr;

mod models;
use self::models::SMResponse;

fn format_arrival_time(arrivaltime: i64) -> String {
    let arrival: DateTime<Local> = Utc.timestamp(arrivaltime, 0).with_timezone(&Local);
    let now: DateTime<Local> = Local::now();

    if (arrival-now).num_minutes() <= 15 {
        format!("{} min", (arrival-now).num_minutes())
    } else {
        format!("{}", arrival.format("%k:%M"))
    }
}

fn convert_encoding(text: String) -> [u8; 20] {
    let mut buffer = [0; 20];
    for (i, codepoint) in text.chars().enumerate().take(20) {
        buffer[i] = match codepoint {
            'Å' => 1,
            'å' => 2,
            'Ä' => 3,
            'ä' => 4,
            'Ö' => 5,
            'ö' => 6,
            _ => codepoint as u8
        };
    }
    buffer
}

fn fetch() -> Vec<[u8;20]> {
    let response: SMResponse = reqwest::get("https://data.foli.fi/siri/sm/583").unwrap().json().unwrap();

    let mut lines = Vec::new();

    for stop in response.result.iter().take(4) {
        let route = stop.lineref.with_exact_width(3);
        let time = format_arrival_time(stop.expectedarrivaltime);
        let name = stop.destinationdisplay.with_exact_width(15-time.len());
        let formatted_line = format!("{} {} {}", route, name, time);
        lines.push(convert_encoding(formatted_line));
    }

    lines
}

fn main() {
    let listener = TcpListener::bind("0.0.0.0:1337").unwrap();
    println!("listening started, ready to accept");
    for stream in listener.incoming() {
        if let Ok(mut stream) = stream {
            for line in fetch() {
                stream.write(&line).unwrap();
                stream.write(&[0]).unwrap();
            }
        }
    }
}